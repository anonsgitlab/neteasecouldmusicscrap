from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from util import getChromeWebDrv, decorateFetch, navigateTo,\
    waitPageLoaded, waitNewPageLoaded, parseSongListTable,\
    getUrlListFromSpans, dlMusicFromUrlList

webDrv = getChromeWebDrv(r"D:\bin\chromedriver_win32\chromedriver.exe")
navigateTo(webDrv, r"https://music.163.com/#/discover/toplist?id=3778678")
decorateFetch(webDrv)

webDrv.switch_to.frame("contentFrame")
waitPageLoaded(webDrv)

songList, spansPlay = parseSongListTable(webDrv, tableType="topList")

urlList = getUrlListFromSpans(webDrv, spansPlay)

dirPath = r"C:\Users\HP\Desktop\网易云音乐"
dlMusicFromUrlList(webDrv, urlList, songList, dirPath)

webDrv.close()