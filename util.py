from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import staleness_of
from selenium.common.exceptions import WebDriverException, TimeoutException
import time, re, os
import urllib.request

def getChromeWebDrv(chromeDrvPath):
    webDrv = webdriver.Chrome(executable_path=chromeDrvPath)
    return webDrv

def decorateFetch(webDrv):
    js_wrapFetch = r'''
            "use strict";
            function getFetchWrapper(fetch) {
                return function () {
                    //console.log("FetchWrapper: arguments:", arguments);
                    if (typeof(window.clickTime)!="undefined" && Date.now()>window.clickTime) {
                        let url = arguments[0];
                        let patt = /^https:\/\/.*\.mp3$/;
                        if (patt.test(url)) {
                            if (typeof(window.lastFetchUrl)=="undefined" ||(typeof(window.lastFetchUrl)!="undefined" && url!=window.lastFetchUrl)) {
                                if (typeof(window.fetchUrl) == "undefined") {
                                    window.fetchUrl = url;
                                    console.log("window.fetchUrl:", window.fetchUrl);
                                }
                            }
                        }
                    }

                    return fetch.apply(this, arguments);
                }
            }

            window.fetch = getFetchWrapper(window.fetch);
            console.log("window.fetch decorated !!!");
        '''
    webDrv.execute_script(js_wrapFetch)

def navigateTo(webDrv, url):
    webDrv.get(url)
    WebDriverWait(webDrv, 100).until(
        lambda webDrv: webDrv.find_element_by_xpath("/html/body/iframe[@name='contentFrame']"))

def ifFileNameValid(fileName):
    patt = re.compile(r'[\\/:*?"<>|]')
    if patt.search(fileName):
        return False
    else:
        return True

def replaceInvalidFileNameChar(fileName, replaceTo="&"):
    return re.sub(r'[\\/:*?"<>|]', replaceTo, fileName, count=0)

def waitUntil(f, timeout, frenquency):
    timeConsumed = 0
    while timeConsumed < timeout:
        if f():
            return "success"
        time.sleep(frenquency)
        timeConsumed += frenquency
    return "timeout"

def waitPageLoaded(webDrv, timeout=100, frequency=0.5):
    timeConsumed = 0
    while timeConsumed < timeout:
        docReadystate = webDrv.execute_script("return document.readyState")
        if docReadystate == "complete":
            return "success"
        else:
            time.sleep(frequency)
            timeConsumed += frequency
    return "timeout"

def waitNewPageLoaded(webDrv, staleTag, timeout=100, frequency=0.5):
    try:
        WebDriverWait(webDrv, timeout).until(staleness_of(staleTag))
    except TimeoutException:
        return "wait stale timeout"
    if waitPageLoaded(webDrv) == "timeout":
        return "wait doc readyState timeout"
    else:
        return "success"

def parseSongListTable(webDrv, tableType):
    webDrv.switch_to.default_content()
    webDrv.switch_to.frame("contentFrame")
    # get songList
    table = webDrv.find_element_by_xpath("//table")
    trs = table.find_elements_by_xpath("tbody/tr")
    songList = []
    cnt = 0
    for tr in trs:
        cnt += 1
        print('parsing %sth tr' % cnt)
        songInfo = {}
        songInfo["sequence"] = tr.find_element_by_xpath("td[1]//span[@class='num']").text
        songInfo["name"] = tr.find_element_by_xpath("td[2]//b[@title]").get_attribute("title")
        songInfo["duration"] = tr.find_element_by_xpath("td[3]/span").text
        if tableType == "topList":
            songInfo["singer"] = tr.find_element_by_xpath("td[4]//span[@title]").get_attribute("title")
        elif tableType == "singerList":
            songInfo["album"] = tr.find_element_by_xpath("td[4]//a[@title]").get_attribute("title")
        songList.append(songInfo)
    # get clickable spans
    spansPlay = webDrv.find_elements_by_xpath(
        "/html/body//table/tbody/tr/td//span[@class='ply' or @class='ply ' or @class='ply ply-z-slt']")
    assert (len(songList) == len(spansPlay))
    return songList, spansPlay

def getUrlListFromSpans(webDrv, spansPlay):
    webDrv.switch_to.default_content()
    webDrv.switch_to.frame("contentFrame")
    js_getUrl = '''
        return function () {
            let url = window.fetchUrl;
            window.lastFetchUrl = window.fetchUrl;
            window.fetchUrl = undefined;
            window.clickTime = undefined;
            return url;
        }();
    '''
    urlList = []
    cnt = 0
    cnt_needPay = 0
    str1 = "版权方要求，当前专辑需单独付费，购买数字专辑即可无限畅享"
    str2 = "版权方要求，当前歌曲仅限开通音乐包使用"
    str3 = "由于版权保护，您所在的地区暂时无法使用。"
    for span in spansPlay:
        cnt += 1
        while True:
            print("getting %sth url" % cnt)
            webDrv.execute_script("arguments[0].scrollIntoView()", span)
            try:
                span.click()
            except WebDriverException:
                div_pay = webDrv.find_element_by_xpath("//div[text()='%s' or text()='%s' or text()='%s']" % (str1, str2, str3))
                url = "cant get url:" + div_pay.text
                print(cnt, url)
                urlList.append(url)
                span_x = webDrv.find_element_by_xpath("//span[@title='关闭窗体']")
                span_x.click()
                cnt_needPay += 1
                break

            webDrv.switch_to.default_content()
            webDrv.execute_script("window.clickTime = Date.now()")

            def checkUrl():
                if (webDrv.execute_script("return window.fetchUrl")):
                    return True
                else:
                    return False

            r = waitUntil(checkUrl, 3, 0.5)
            if r == "success":
                url = webDrv.execute_script(js_getUrl)
                urlList.append(url)
                print(cnt, url)
                webDrv.switch_to.frame("contentFrame")
                break

            webDrv.switch_to.frame("contentFrame")
    print("%s songs need to pay" % cnt_needPay)
    return urlList

def dlMusicFromUrlList(webDrv, urlList, songList, dirPath):
    headers = {'Referer': 'https://music.163.com/',
               'Origin': 'https://music.163.com',
               'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36',
               'Range': 'bytes=0-'
               }
    index = 0
    for url in urlList:
        if re.search("^cant get url:", url):
            print("cant download %sth url:", url)
            index += 1
            continue
        request = urllib.request.Request(url, headers=headers)
        response = urllib.request.urlopen(request)
        # fileName = "%s_%s_%s.mp3" % (songList[index]['name'],
        #                              songList[index]['duration'].replace(':', '-'),
        #                              songList[index]['singer'].replace('/', '-'))
        fileName = "%s.mp3" % songList[index]['name']
        if not ifFileNameValid(fileName):
            fileName = replaceInvalidFileNameChar(fileName)
        filePath = os.path.join(dirPath, fileName)
        with open(filePath, "wb") as file:
            file.write(response.read())
        index += 1
        print('downloaded %sth file' % index)
        time.sleep(1)

